const express = require('express');
const cors = require('cors');
const routes = require ('./routes');

const app = express();
app.use(cors());
app.use(express.json());
app.use(routes);




/**
 * Métodos HTTP
 * 
 * GET: Buscar uma informação do Bacl-end
 * POST: Criar uma informação no Back-end
 * PUT: Alterar uma informação no Back-end
 * Delete: Deletar uma informação no back-end
 */

 /**
  * Tipos de Parâmetros:
  * 
  * Query Params: Parâmetros nomeados enviados na rota após "?" (Filtros, Paginação)
  * Route Params: Parâmetros utilizados para indentificar recursos
  * Request Body: Corpo da requisição, utilizafo para criar ou alterar recursos
  */

  /**
   * AQL: MySQL, SQLite, Oracle, Microsoft SQL Server
   * NoSQL: MongoDB, CouchDB, etc
   */

   /**
    * Driver:  SELECT*FROM users
    * Query: Builder: table('users).select('*').where()
    */

app.listen(3333);